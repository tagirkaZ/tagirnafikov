//
//  JSONableModelBehavior.h
//  BlockChecker
//
//  Created by Tagir Nafikov on 06/11/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

/*--------------------------------------------------*/

#import <Foundation/Foundation.h>

/*--------------------------------------------------*/

@protocol JSONModelBehavior <NSObject>

+ (NSDictionary *)jsonMap;

@end

/*--------------------------------------------------*/
