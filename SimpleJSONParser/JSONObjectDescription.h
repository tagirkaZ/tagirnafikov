//
//  JSONObjectDescription.h
//  QRChain
//
//  Created by Tagir Nafikov on 17.09.16.
//  Copyright © 2016 SOFT DDS. All rights reserved.
//

/*--------------------------------------------------*/

#import "JSONValueDescription.h"

/*--------------------------------------------------*/

@interface JSONObjectDescription : JSONValueDescription

@property (nonatomic, copy) NSString *className;

- (instancetype)initWithClassName:(NSString *)className jsonKey:(NSString *)jsonKey;
+ (instancetype)descriptionWithClassName:(NSString *)className jsonKey:(NSString *)jsonKey;

@end

/*--------------------------------------------------*/
