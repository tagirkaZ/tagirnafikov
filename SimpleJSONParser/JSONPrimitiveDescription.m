//
//  JSONPrimitiveDescription.m
//  BlockChecker
//
//  Created by Tagir Nafikov on 05/11/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

#import "JSONPrimitiveDescription.h"

@implementation JSONPrimitiveDescription

#pragma mark - Init

- (instancetype)initWithPrimitiveType:(JSONPrimitiveDescriptionPrimitiveType)primitiveType jsonKey:(NSString *)jsonKey {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _primitiveType = primitiveType;
    self.jsonKey = jsonKey;
    
    return self;
}

+ (instancetype)descriptionWithPrimitiveType:(JSONPrimitiveDescriptionPrimitiveType)primitiveType jsonKey:(NSString *)jsonKey {
    return [[self alloc] initWithPrimitiveType:primitiveType jsonKey:jsonKey];
}

#pragma mark - Setters/Getters

- (BOOL)isValid {
    return self.primitiveType != JSONPrimitiveDescriptionPrimitiveTypeUndefined && self.jsonKey.length;
}

@end
