//
//  JSONObjectDescription.m
//  QRChain
//
//  Created by Tagir Nafikov on 17.09.16.
//  Copyright © 2016 SOFT DDS. All rights reserved.
//

/*--------------------------------------------------*/

#import "JSONObjectDescription.h"

/*--------------------------------------------------*/
#pragma mark -
/*--------------------------------------------------*/

@implementation JSONObjectDescription

#pragma mark - Init

- (instancetype)initWithClassName:(NSString *)className jsonKey:(NSString *)jsonKey {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _className = className;
    self.jsonKey = jsonKey;
    
    return self;
}

+ (instancetype)descriptionWithClassName:(NSString *)className jsonKey:(NSString *)jsonKey {
    return [[self alloc] initWithClassName:className jsonKey:jsonKey];
}

#pragma mark - Setters/Getters

- (BOOL)isValid {
    return self.className.length && self.jsonKey.length;
}

@end
