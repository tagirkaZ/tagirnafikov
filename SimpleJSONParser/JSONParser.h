//
//  JSONParser.h
//  QRChain
//
//  Created by Tagir Nafikov on 17.09.16.
//  Copyright © 2016 SOFT DDS. All rights reserved.
//

/*--------------------------------------------------*/

#import <Foundation/Foundation.h>

/*--------------------------------------------------*/

@interface JSONParser : NSObject

+ (id)modelForClassName:(NSString *)className fromJSON:(NSDictionary *)jsonDict;

@end

/*--------------------------------------------------*/
