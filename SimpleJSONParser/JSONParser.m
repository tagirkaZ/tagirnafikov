//
//  JSONParser.m
//  QRChain
//
//  Created by Tagir Nafikov on 17.09.16.
//  Copyright © 2016 SOFT DDS. All rights reserved.
//

/*--------------------------------------------------*/

#import "JSONParser.h"

/*--------------------------------------------------*/

#import "JSONModelBehavior.h"
#import "JSONObjectDescription.h"
#import "JSONPrimitiveDescription.h"

/*--------------------------------------------------*/
#pragma mark -
/*--------------------------------------------------*/

@implementation JSONParser

#pragma mark - Create objects from json

+ (id)modelForClassName:(NSString *)className fromJSON:(NSDictionary *)jsonDict {
    Class aClass = NSClassFromString(className);
    
    if (![aClass conformsToProtocol:@protocol(JSONModelBehavior)]) {
        return nil;
    }
    
    NSDictionary *jsonMap = [aClass jsonMap];
    id <JSONModelBehavior> model = [aClass new];
    
    for (NSString *key in jsonMap.allKeys) {
        if (![model respondsToSelector:NSSelectorFromString(key)]) {
            continue;
        }
        
        JSONValueDescription *valueDescription = jsonMap[key];
        
        if (![valueDescription isKindOfClass:[JSONValueDescription class]]) {
            continue;
        }
        if (!valueDescription.isValid) {
            continue;
        }
        
        [self setValueToModel:model forKey:key fromValueDescription:valueDescription fromJSON:jsonDict];
    }
    
    return model;
}

+ (void)setValueToModel:(id)model forKey:(NSString *)key fromValueDescription:(JSONValueDescription *)valueDescription fromJSON:(NSDictionary *)jsonDict {
    if ([valueDescription isKindOfClass:[JSONObjectDescription class]]) {
        JSONObjectDescription *objectDescription = (JSONObjectDescription *)valueDescription;
        
        Class class = NSClassFromString(objectDescription.className);
        if (!class) {
            return;
        }
        
        NSString *jsonKey = objectDescription.jsonKey;
        id rawValue = jsonDict[jsonKey];
        [model setValue:[self valueForClass:class fromRawValue:rawValue] forKey:key];
    }
    else if ([valueDescription isKindOfClass:[JSONPrimitiveDescription class]]) {
        JSONPrimitiveDescription *primitiveDescription = (JSONPrimitiveDescription *)valueDescription;
        
        JSONPrimitiveDescriptionPrimitiveType primitivieType = primitiveDescription.primitiveType;
        if (primitivieType == JSONPrimitiveDescriptionPrimitiveTypeUndefined) {
            return;
        }
        
        NSString *jsonKey = valueDescription.jsonKey;
        
        id rawValue = jsonDict[jsonKey];
        
        if (primitivieType == JSONPrimitiveDescriptionPrimitiveTypeFloat) {
            if (![rawValue respondsToSelector:@selector(floatValue)]) {
                return;
            }
            
            [model setValue:@([rawValue floatValue]) forKey:key];
        }
        else if (primitivieType == JSONPrimitiveDescriptionPrimitiveTypeInteger) {
            if (![rawValue respondsToSelector:@selector(integerValue)]) {
                return;
            }
            
            [model setValue:@([rawValue integerValue]) forKey:key];
        }
    }

}

+ (id)valueForClass:(Class)class fromRawValue:(id)rawValue {
    id value = nil;
    if (!rawValue) {
        return nil;
    }
    
    if ([rawValue isKindOfClass:[NSDictionary class]]) {
        if ([class conformsToProtocol:@protocol(JSONModelBehavior)]) {
            value = [self modelForClassName:NSStringFromClass(class) fromJSON:rawValue];
            
            if (![value isKindOfClass:class]) {
                return nil;
            }
        }
    }
    else if ([rawValue isKindOfClass:[NSArray class]]) {
        value = [self arrayOfValuesForClass:class fromJsonArray:rawValue];
    }
    else if ([rawValue isKindOfClass:class]) {
        value = rawValue;
    }
    
    return value;
}

+ (NSArray *)arrayOfValuesForClass:(Class)class fromJsonArray:(NSArray *)jsonArray {
    NSMutableArray *models = [[NSMutableArray alloc] initWithCapacity:jsonArray.count];
    
    for (id jsonModel in jsonArray) {
        id model = [self modelForClassName:NSStringFromClass(class) fromJSON:jsonModel];
        
        if (model) {
            [models addObject:model];
        }
    }

    return models.copy;
}

@end
