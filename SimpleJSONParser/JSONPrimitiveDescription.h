//
//  JSONPrimitiveDescription.h
//  BlockChecker
//
//  Created by Tagir Nafikov on 05/11/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

/*--------------------------------------------------*/

#import "JSONValueDescription.h"

/*--------------------------------------------------*/

typedef NS_ENUM(NSInteger, JSONPrimitiveDescriptionPrimitiveType) {
    JSONPrimitiveDescriptionPrimitiveTypeUndefined,
    JSONPrimitiveDescriptionPrimitiveTypeInteger,
    JSONPrimitiveDescriptionPrimitiveTypeFloat
};

/*--------------------------------------------------*/

@interface JSONPrimitiveDescription : JSONValueDescription

@property (nonatomic) JSONPrimitiveDescriptionPrimitiveType primitiveType;

- (instancetype)initWithPrimitiveType:(JSONPrimitiveDescriptionPrimitiveType)primitiveType jsonKey:(NSString *)jsonKey;
+ (instancetype)descriptionWithPrimitiveType:(JSONPrimitiveDescriptionPrimitiveType)primitiveType jsonKey:(NSString *)jsonKey;

@end
