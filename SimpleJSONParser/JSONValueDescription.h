//
//  JSONValueDescription.h
//  BlockChecker
//
//  Created by Tagir Nafikov on 05/11/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

/*--------------------------------------------------*/

#import <Foundation/Foundation.h>

/*--------------------------------------------------*/

@interface JSONValueDescription : NSObject

@property (nonatomic, copy) NSString *jsonKey;
@property (nonatomic, readonly) BOOL isValid;

@end

/*--------------------------------------------------*/
