//
//  PersistencyManager.m
//  BlockChecker
//
//  Created by Tagir Nafikov on 16.10.16.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

#import "PersistencyManager.h"
#import "ConstantsAndHelpers.h"
#import <Realm/Realm.h>

#import "NSObject+Validation.h"

static NSString *const kDataStoreManagerClassName = @"kDataStoreManagerClassName";

/*--------------------------------------------------*/

@interface PersistencyManager()

@end

/*--------------------------------------------------*/

@implementation PersistencyManager

#pragma mark - Singleton

+ (instancetype)shared {
    static id shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [self new];
    });
    
    return shared;
}

#pragma mark - Public class methods

+ (NSString *)getUUID {
    return [[NSUUID UUID] UUIDString];
}

#pragma mark - Persistency

- (void)addOrUpdateObject:(id)object {
    RLMObject *objectRLM = [self realmObjectFromObject:object];
    
    if (!objectRLM) {
        return;
    }
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:objectRLM];
    [realm commitWriteTransaction];
}

- (void)addObject:(id)object {
    RLMObject *objectRLM = [self realmObjectFromObject:object];
    
    if (!objectRLM) {
        return;
    }
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm addObject:objectRLM];
    [realm commitWriteTransaction];
}

- (void)addOrUpdateRealmObject:(RLMObject *)object {
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:object];
    [realm commitWriteTransaction];
}

- (void)addRealmObject:(RLMObject *)object {
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm addObject:object];
    [realm commitWriteTransaction];
}

#pragma mark - Objects converting

- (id)objectFromRealmObject:(RLMObject *)rlmObj {
    if (!rlmObj) {
        return nil;
    }
    
    RLMObjectSchema *schema = rlmObj.objectSchema;
    
    NSString *className = schema.className;
    className = [className stringByReplacingCharactersInRange:NSMakeRange(className.length - 3, 3) withString:@""];
    
    id object = [[NSClassFromString(className) alloc] init];
    
    if (!object) {
        return nil;
    }
    
    for (RLMProperty *property in schema.properties) {
        if (![object respondsToSelector:NSSelectorFromString(property.name)]) {
            continue;
        }
        
        if (property.type == RLMPropertyTypeObject) {
            RLMObject *propertyObjectRLM = [rlmObj valueForKey:property.name];
            
            if (propertyObjectRLM) {
                id propertyObject = [self objectFromRealmObject:propertyObjectRLM];
                
                if (propertyObject) {
                    [object setValue:propertyObject forKey:property.name];
                }
            }
        }
        else if (property.type == RLMPropertyTypeArray) {
            RLMArray *propertyArrayRLM = [rlmObj valueForKey:property.name];
            
            if (propertyArrayRLM) {
                NSMutableArray *propertyArray = [[NSMutableArray alloc] initWithCapacity:propertyArrayRLM.count];
                
                for (RLMObject *propertyObjectRLM in propertyArrayRLM) {
                    id propertyObject = [self objectFromRealmObject:propertyObjectRLM];
                    
                    if (propertyObject) {
                        [propertyArray addObject:propertyObject];
                    }
                }
                
                [object setValue:propertyArray forKey:property.name];
            }
        }
        else {
            id propertyValue = [rlmObj valueForKey:property.name];
            
            if (propertyValue) {
                [object setValue:propertyValue forKey:property.name];
            }
        }
    }
    
    return object;
}

- (id)realmObjectFromObject:(NSObject *)obj {
    if (!obj) {
        return nil;
    }
    
    NSString* className = NSStringFromClass(obj.class);
    className = [className stringByAppendingString:@"RLM"];
    
    id rlmObject = [[NSClassFromString(className) alloc] init];
    
    if (!rlmObject) {
        return nil;
    }
    
    RLMObjectSchema *schema = [rlmObject valueForKey:@"objectSchema"];
    
    for (RLMProperty *property in schema.properties) {
        if (![obj respondsToSelector:NSSelectorFromString(property.name)]) {
            continue;
        }
        
        if (property.type == RLMPropertyTypeObject) {
            id propertyObject = [obj valueForKey:property.name];
            
            if (propertyObject) {
                RLMObject *propertyObjectRealm = [self realmObjectFromObject:propertyObject];
                
                if (propertyObjectRealm) {
                    [rlmObject setValue:propertyObjectRealm forKey:property.name];
                }
            }
        }
        else if (property.type == RLMPropertyTypeArray) {
            NSArray *propertyArrayObj = [obj valueForKey:property.name];
            
            if (propertyArrayObj) {
                for (id propertyObject in propertyArrayObj) {
                    RLMObject *propertyObjectRealm = [self realmObjectFromObject:propertyObject];
                    
                    if (propertyObjectRealm) {
                        RLMArray *rlmArray = [rlmObject valueForKey:property.name];
                        [rlmArray addObject:propertyObjectRealm];
                    }
                }
            }
        }
        else {
            id propertyValue = [obj valueForKey:property.name];
            
            if (propertyValue) {
                [rlmObject setValue:propertyValue forKey:property.name];
            }
        }
    }
    
    return rlmObject;
}

- (NSDictionary *)objectDictFromRealmObject:(RLMObject *)object {
    if (!object) {
        return nil;
    }
    
    RLMObjectSchema *schema = object.objectSchema;
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:(schema.properties.count + 1)];
    
    dict[kDataStoreManagerClassName] = NSStringFromClass(object.class);
    
    for (RLMProperty *property in schema.properties) {
        if (property.type == RLMPropertyTypeObject) {
            RLMObject *propertyObject = [object valueForKey:property.name];
            
            if (propertyObject) {
                NSDictionary *propertyObjectDict = [self objectDictFromRealmObject:propertyObject];
                if (propertyObjectDict) {
                    dict[property.name] = propertyObjectDict;
                }
            }
        }
        else if (property.type == RLMPropertyTypeArray) {
            RLMArray *propertyArray = [object valueForKey:property.name];
            
            if (propertyArray) {
                NSMutableArray *propertyObjectsArray = [[NSMutableArray alloc] initWithCapacity:propertyArray.count];
                
                for (RLMObject *propertyObject in propertyArray) {
                    NSDictionary *propertyObjectDict = [self objectDictFromRealmObject:propertyObject];
                    
                    if (propertyObjectDict) {
                        [propertyObjectsArray addObject:propertyObjectDict];
                    }
                }
                
                if (propertyObjectsArray.count > 0) {
                    dict[property.name] = propertyObjectsArray;
                }
            }
        }
        else if (property.type == RLMPropertyTypeDate) {
            id propertyValue = [object valueForKey:property.name];
            
            if (propertyValue) {
                dict[property.name] = @([propertyValue timeIntervalSince1970]);
            }
        }
        else {
            id propertyValue = [object valueForKey:property.name];
            
            if (propertyValue) {
                dict[property.name] = propertyValue;
            }
        }
    }
    
    return dict.copy;
}

- (RLMObject *)realmObjectFromDict:(NSDictionary *)objectDict {
    if (![objectDict validation_isNotEmptyDictionary]) {
        return nil;
    }
    
    NSString *className = objectDict[kDataStoreManagerClassName];
    if (![className validation_isNotEmptyString]) {
        return nil;
    }
    
    RLMObject *rlmObject = [[NSClassFromString(className) alloc] init];
    RLMObjectSchema *schema = rlmObject.objectSchema;
    
    for (RLMProperty *property in schema.properties) {
        if (property.type == RLMPropertyTypeObject) {
            id propertyObjectDict = objectDict[property.name];
            
            if (propertyObjectDict) {
                RLMObject *propertyRLMObject = [self realmObjectFromDict:propertyObjectDict];
                
                if (propertyRLMObject) {
                    [rlmObject setValue:propertyRLMObject forKey:property.name];
                }
            }
        }
        else if (property.type == RLMPropertyTypeArray) {
            NSArray *propertyArray = objectDict[property.name];
            
            if (propertyArray) {
                for (id propertyObjectDict in propertyArray) {
                    RLMObject *propertyObjectRealm = [self realmObjectFromDict:propertyObjectDict];
                    
                    if (propertyObjectRealm) {
                        RLMArray *rlmArray = [rlmObject valueForKey:property.name];
                        [rlmArray addObject:propertyObjectRealm];
                    }
                }
            }
        }
        else if (property.type == RLMPropertyTypeDate) {
            id value = objectDict[property.name];
            
            if ([value respondsToSelector:@selector(doubleValue)]) {
                NSTimeInterval timeInterval = [value doubleValue];
                [rlmObject setValue:[NSDate dateWithTimeIntervalSince1970:timeInterval] forKey:property.name];
            }
        }
        else {
            id value = objectDict[property.name];
            
            if (value) {
                [rlmObject setValue:value forKey:property.name];
            }
        }
    }
    
    return rlmObject;
}

- (NSArray *)objectsArrayFromRLMResults:(RLMResults *)results {
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:results.count];
    
    for (RLMObject *rlmObject in results) {
        id object = [self objectFromRealmObject:rlmObject];
        
        if (object) {
            [array addObject:object];
        }
    }
    
    return array.copy;
}

- (NSArray *)jsonObjectsArrayFromRLMResults:(RLMResults *)results {
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:results.count];
    
    for (RLMObject *rlmObject in results) {
        id objectDict = [self objectDictFromRealmObject:rlmObject];
        
        if ([objectDict validation_isDictionary]) {
            [array addObject:objectDict];
        }
    }
    
    return array.copy;
}

@end
