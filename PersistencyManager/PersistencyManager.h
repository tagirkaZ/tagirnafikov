//
//  PersistencyManager.h
//  BlockChecker
//
//  Created by Tagir Nafikov on 16.10.16.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

/*--------------------------------------------------*/

#import <Foundation/Foundation.h>

/*--------------------------------------------------*/

@interface PersistencyManager : NSObject

+ (instancetype)shared;

+ (NSString *)getUUID;

- (void)addOrUpdateObject:(id)object;
- (void)addObject:(id)object;

@end
