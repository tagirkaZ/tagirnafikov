// Shows direction to qibla

#import "QiblaDirection.h"
#import <Foundation/Foundation.h>

typedef NS_ENUM(int, DirectionUpdatingAvailability) {
    DirectionUpdatingNotDetermined,
    DirectionUpdatingRestricted,
    DirectionUpdatingDenied,
    DirectionUpdatingAuthorized,
    DirectionUpdatingUnavailable
};

extern NSString *const kNotificationNameQiblaDirectionUpdated;
extern NSString *const kNotificationNameLocationUpdated;
extern NSString *const kNotificationNameDirectionUpdatingStatusChanged;

@interface QiblaManager : NSObject

@property (nonatomic, getter=isUpdating) BOOL updating;

+ (QiblaManager *)sharedManager;
+ (DirectionUpdatingAvailability)directionUpdatingAvailability;

- (void)startUpdatingDirection;
- (void)stopUpdatingDirection;
- (void)pauseUpdatingDirection;
- (void)resumeUpdatingDirection;

@end
