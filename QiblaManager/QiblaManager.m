// Shows direction to qibla

#import "QiblaManager.h"
#import "SharedClass.h"
#import "TeoriusHelper.h"
#import <CoreLocation/CoreLocation.h>

#define MEKKA_LAT 21.418834f
#define MEKKA_LON 39.816856f

NSString *const kNotificationNameQiblaDirectionUpdated = @"kNotificationNameQiblaDirectionUpdated";
NSString *const kNotificationNameLocationUpdated = @"kNotificationNameLocationUpdated";
NSString *const kNotificationNameDirectionUpdatingStatusChanged = @"kNotificationNameDirectionUpdatingStatusChanged";

@interface QiblaManager() <CLLocationManagerDelegate>

@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *location;
@property (nonatomic) NSTimer *updatingLocationTimer;
@property (nonatomic, getter=isPaused) BOOL paused;

@end

@implementation QiblaManager


#pragma mark - Shared Instance

+ (QiblaManager *)sharedManager {
    static QiblaManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [self new];
        manager.updating = NO;
    });
    
    return manager;
}


#pragma mark - Setters/Getters

- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    
    return _locationManager;
}


#pragma mark - Class methods

+ (DirectionUpdatingAvailability)directionUpdatingAvailability {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (![CLLocationManager significantLocationChangeMonitoringAvailable] || ![CLLocationManager headingAvailable]) {
        return DirectionUpdatingUnavailable;
    }
    if (status == kCLAuthorizationStatusNotDetermined) {
        return DirectionUpdatingNotDetermined;
    }
    else if (status == kCLAuthorizationStatusRestricted) {
        return DirectionUpdatingRestricted;
    }
    else if (status == kCLAuthorizationStatusDenied) {
        return DirectionUpdatingDenied;
    }
    else {
        return DirectionUpdatingAuthorized;
    }
}


#pragma mark - Interface

- (void)startUpdatingDirection {
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
    [self.locationManager startUpdatingHeading];
    
    [self startUpdatingLocationTimer];
    
    self.updating = YES;
}

- (void)stopUpdatingDirection {
    [self.locationManager stopUpdatingLocation];
    [self.locationManager stopUpdatingHeading];
    
    [self stopUpdatingLocationTimer];
    
    self.updating = NO;
}

- (void)pauseUpdatingDirection {
    self.paused = YES;
}

- (void)resumeUpdatingDirection {
    self.paused = NO;
}


#pragma mark - Location manager delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.location = locations.lastObject;
    [SharedClass sharedClassInstance].locationUpdated = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameLocationUpdated object:self.location];
    
    [self stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    if (!self.location) {
        return;
    }
    
    [SharedClass sharedClassInstance].headingUpdated = YES;
    
    // Use the true heading if it is valid.
    CLLocationDirection currentHeading = ((newHeading.trueHeading > 0) ? newHeading.trueHeading : newHeading.magneticHeading);
    CLLocationDirection cityHeading = [self directionFrom:self.location.coordinate];

    float qiblaRad = (float)([TeoriusHelper degreesToRadians:cityHeading] - [TeoriusHelper degreesToRadians:currentHeading]);
    float northRad =  (float)(-currentHeading * M_PI / 180.0f);

    QiblaDirection *direction = [QiblaDirection new];
    direction.northAngle = northRad;
    direction.qiblaAngle = qiblaRad;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameQiblaDirectionUpdated object:direction];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameDirectionUpdatingStatusChanged object:nil];
}


#pragma mark - Calculation

- (CLLocationDirection)directionFrom:(CLLocationCoordinate2D) startPt {
    double lat1 = [TeoriusHelper degreesToRadians:startPt.latitude];
    double lat2 = [TeoriusHelper degreesToRadians:MEKKA_LAT];
    double lon1 = [TeoriusHelper degreesToRadians:startPt.longitude];
    double lon2 = [TeoriusHelper degreesToRadians:MEKKA_LON];
    double dLon = (lon2-lon1);
    
    double y = sin(dLon) * cos(lat2);
    double x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
    double brng = [TeoriusHelper radiansToDegrees:atan2(y, x)];
    
    brng = (brng+360);
    brng = (brng>360)? (brng-360) : brng;
    
    return brng;
}


#pragma mark - Updating location

- (void)startUpdatingLocation {
    if (!self.isPaused) {
        [self.locationManager startUpdatingLocation];
    }
}

- (void)stopUpdatingLocation {
    [self.locationManager stopUpdatingLocation];
}

- (void)startUpdatingLocationTimer {
    if (self.updatingLocationTimer) {
        [self.updatingLocationTimer invalidate];
    }
    
    self.updatingLocationTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(startUpdatingLocation) userInfo:nil repeats:YES];
}

- (void)stopUpdatingLocationTimer {
    [self.updatingLocationTimer invalidate];
    self.updatingLocationTimer = nil;
}

@end
