//
//  NSObject+Validation.m
//  BlockChecker
//
//  Created by Tagir Nafikov on 12/11/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

#import "NSObject+Validation.h"

@implementation NSObject (Validation)

- (BOOL)validation_isArray {
    if (![self isKindOfClass:[NSArray class]]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validation_isNotEmptyArray {
    if (![self validation_isArray]) {
        return NO;
    }
    NSArray *array = (NSArray *)self;
    if (!array.count) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validation_isDictionary {
    if (![self isKindOfClass:[NSDictionary class]]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validation_isNotEmptyDictionary {
    if (![self validation_isDictionary]) {
        return NO;
    }
    
    NSDictionary *dict = (NSDictionary *)self;
    if (!dict.count) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validation_isData {
    if (![self isKindOfClass:[NSData class]]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validation_isNotEmptyData {
    if (![self validation_isData]) {
        return NO;
    }
    
    NSData *data = (NSData *)self;
    if (!data.length) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validation_isString {
    if (![self isKindOfClass:[NSString class]]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validation_isNotEmptyString {
    if (![self validation_isString]) {
        return NO;
    }
    
    NSString *string = (NSString *)self;
    if (!string.length) {
        return NO;
    }
    
    return YES;
}

@end
