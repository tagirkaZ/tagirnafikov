//
//  NSObject+Validation.h
//  BlockChecker
//
//  Created by Tagir Nafikov on 12/11/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Validation)

- (BOOL)validation_isArray;
- (BOOL)validation_isNotEmptyArray;
- (BOOL)validation_isDictionary;
- (BOOL)validation_isNotEmptyDictionary;
- (BOOL)validation_isData;
- (BOOL)validation_isNotEmptyData;
- (BOOL)validation_isString;
- (BOOL)validation_isNotEmptyString;

@end
