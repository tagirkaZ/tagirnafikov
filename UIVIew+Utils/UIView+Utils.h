/*--------------------------------------------------*/

#import <UIKit/UIKit.h>

/*--------------------------------------------------*/

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(degrees) ((M_PI * degrees) / 180.0)

/*--------------------------------------------------*/

@interface UIView (Utils)

@property (nonatomic, readonly) CGPoint utils_localCenter;
@property (nonatomic) CGFloat utils_width;
@property (nonatomic) CGFloat utils_height;
@property (nonatomic, readonly) CGFloat utils_halfWidth;
@property (nonatomic, readonly) CGFloat utils_halfHeight;
@property (nonatomic) CGFloat utils_maxX;
@property (nonatomic) CGFloat utils_maxY;
@property (nonatomic) CGFloat utils_minX;
@property (nonatomic) CGFloat utils_minY;
@property (nonatomic) CGSize utils_size;
@property (nonatomic) CGPoint utils_origin;
@property (nonatomic) CGFloat utils_x;
@property (nonatomic) CGFloat utils_y;
@property (nonatomic) CGFloat utils_centerX;
@property (nonatomic) CGFloat utils_centerY;
@property (nonatomic) CGPoint utils_topRightCorner;
@property (nonatomic) CGPoint utils_bottomLeftCorner;
@property (nonatomic) CGPoint utils_bottomRightCorner;

- (UIImage *)utils_imageRepresentation;
- (CGFloat)rotationAngle;

@end

/*--------------------------------------------------*/
