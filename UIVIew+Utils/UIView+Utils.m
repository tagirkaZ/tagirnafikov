/*--------------------------------------------------*/

#import "UIView+Utils.h"

@implementation UIView (Utils)

#pragma mark - Frame/Position

- (CGPoint)utils_localCenter {
    return CGPointMake(self.frame.size.width / 2.f, self.frame.size.height / 2.f);
}

- (CGSize)utils_size {
    return self.frame.size;
}

- (void)setUtils_size:(CGSize)utils_size {
    self.frame = (CGRect){self.utils_origin, utils_size};
}

- (CGPoint)utils_origin {
    return self.frame.origin;
}

- (void)setUtils_origin:(CGPoint)utils_origin {
    self.frame = (CGRect){utils_origin, self.utils_size};
}

- (CGFloat)utils_x {
    return self.utils_origin.x;
}

- (void)setUtils_x:(CGFloat)utils_x {
    self.frame = (CGRect){CGPointMake(utils_x, self.utils_y), self.utils_size};
}

- (CGFloat)utils_y {
    return self.utils_origin.y;
}

- (void)setUtils_y:(CGFloat)utils_y {
    self.frame = (CGRect){CGPointMake(self.utils_x, utils_y), self.utils_size};
}

- (CGFloat)utils_width {
    return self.frame.size.width;
}

- (void)setUtils_width:(CGFloat)utils_width {
    self.frame = (CGRect){self.utils_origin, CGSizeMake(utils_width, self.utils_height)};
}

- (CGFloat)utils_height {
    return self.frame.size.height;
}

- (void)setUtils_height:(CGFloat)utils_height {
    self.frame = (CGRect){self.utils_origin, CGSizeMake(self.utils_width, utils_height)};
}

- (CGFloat)utils_halfWidth {
    return self.frame.size.width / 2.f;
}

- (CGFloat)utils_halfHeight {
    return self.frame.size.height / 2.f;
}

- (CGFloat)utils_maxX {
    return CGRectGetMaxX(self.frame);
}

- (void)setUtils_maxX:(CGFloat)utils_maxX {
    self.utils_x = utils_maxX - self.utils_width;
}

- (CGFloat)utils_maxY {
    return CGRectGetMaxY(self.frame);
}

- (void)setUtils_maxY:(CGFloat)utils_maxY {
    self.utils_y = utils_maxY - self.utils_height;
}

- (CGFloat)utils_minX {
    return CGRectGetMinX(self.frame);
}

- (void)setUtils_minX:(CGFloat)utils_minX {
    self.utils_x = utils_minX;
}

- (CGFloat)utils_minY {
    return CGRectGetMinY(self.frame);
}

- (void)setUtils_minY:(CGFloat)utils_minY {
    self.utils_y = utils_minY;
}

- (CGFloat)utils_centerX {
    return self.center.x;
}

- (void)setUtils_centerX:(CGFloat)utils_centerX {
    self.center = CGPointMake(utils_centerX, self.utils_centerY);
}

- (CGFloat)utils_centerY {
    return self.center.y;
}

- (void)setUtils_centerY:(CGFloat)utils_centerY {
    self.center = CGPointMake(self.utils_centerX, utils_centerY);
}

- (CGPoint)utils_topRightCorner {
    return CGPointMake(self.utils_maxX, self.utils_minY);
}

- (void)setUtils_topRightCorner:(CGPoint)utils_topRightCorner {
    self.utils_origin = CGPointMake(utils_topRightCorner.x - self.utils_width, utils_topRightCorner.y);
}

- (CGPoint)utils_bottomLeftCorner {
    return CGPointMake(self.utils_minX, self.utils_maxY);
}

- (void)setUtils_bottomLeftCorner:(CGPoint)utils_bottomLeftCorner {
    self.utils_origin = CGPointMake(utils_bottomLeftCorner.x, utils_bottomLeftCorner.y - self.utils_height);
}

- (CGPoint)utils_bottomRightCorner {
    return CGPointMake(self.utils_maxX, self.utils_maxY);
}

- (void)setUtils_bottomRightCorner:(CGPoint)utils_bottomRightCorner {
    self.utils_origin = CGPointMake(utils_bottomRightCorner.x - self.utils_width, utils_bottomRightCorner.y - self.utils_height);
}

#pragma mark - Image

- (UIImage *)utils_imageRepresentation {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, [[UIScreen mainScreen] scale]);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

#pragma mark - Other

- (CGFloat)rotationAngle {
    CGAffineTransform transform = self.transform;
    return RADIANS_TO_DEGREES(atan2(transform.b, transform.a));
}

@end
