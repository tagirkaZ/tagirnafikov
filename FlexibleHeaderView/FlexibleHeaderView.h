//
//  FlexibleHeaderView.h
//  BlockChecker
//
//  Created by Tagir Nafikov on 26/11/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlexibleHeaderView;

@protocol FlexibleHeaderViewDelegate <NSObject>

- (void)flexibleHeaderView:(FlexibleHeaderView *)view willChangeHeightTo:(CGFloat)newHeight animatedWithDuration:(CGFloat)duration;

@end

@interface FlexibleHeaderView : UIView

@property (nonatomic, readonly) UILabel *mainLabel;
@property (nonatomic, readonly) UILabel *secondaryLabel;
@property (nonatomic, readonly) UITableView *tableView;
@property (nonatomic) UIEdgeInsets contentInsets;

@property (nonatomic, weak) id <FlexibleHeaderViewDelegate> delegate;

@end
