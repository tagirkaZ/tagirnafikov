//
//  FlexibleHeaderView.m
//  BlockChecker
//
//  Created by Tagir Nafikov on 26/11/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

/*--------------------------------------------------*/

#import "FlexibleHeaderView.h"

/*--------------------------------------------------*/

#import "UIView+Utils.h"
#import "CALayer+Utils.h"
#import "UIButton+HitArea.h"

/*--------------------------------------------------*/

#import "RotatableImageButton.h"

/*--------------------------------------------------*/

static const NSInteger kSecondaryLabelTopSpace = 22;
static const NSInteger kVerticalSpace = 14;

@interface FlexibleHeaderView()

@property (nonatomic) BOOL initialLayoutWasSet;
@property (nonatomic) BOOL showingAllInfo;

@property (nonatomic) RotatableImageButton *showMoreButton;

@end

/*--------------------------------------------------*/

@implementation FlexibleHeaderView

#pragma mark - Init

- (instancetype)init {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    return [self baseInit];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (!self) {
        return nil;
    }
    
    return [self baseInit];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (!self) {
        return nil;
    }
    
    return [self baseInit];
}

- (instancetype)baseInit {
    self.backgroundColor = [UIColor clearColor];
    
    _mainLabel = [UILabel new];
    _mainLabel.numberOfLines = 0;
    [self addSubview:_mainLabel];
    
    _secondaryLabel = [UILabel new];
    _secondaryLabel.numberOfLines = 0;
    [self addSubview:_secondaryLabel];
    
    _showMoreButton = [RotatableImageButton buttonWithType:UIButtonTypeCustom];
    [_showMoreButton setTitle:@"Подробнее" forRotatableImageButtonState:RotatableImageButtonStateDefault];
    [_showMoreButton setTitle:@"Скрыть" forRotatableImageButtonState:RotatableImageButtonStateAlternative];
    [_showMoreButton setTitleColor:[UIColor colorWithWhite:1.f alpha:.5f] forState:UIControlStateNormal];
    _showMoreButton.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:13.f];
    _showMoreButton.rotatableImageView.image = [UIImage imageNamed:@"arrowDown"];
    [_showMoreButton addTarget:self action:@selector(showMoreButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    _showMoreButton.hitTestEdgeInsets = UIEdgeInsetsMake(-12.f, -12.f, -12.f, -12.f);
    [self addSubview:_showMoreButton];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.scrollEnabled = NO;
    _tableView.scrollsToTop = NO;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.alpha = 0;
    [self addSubview:_tableView];
    
    return self;
}

#pragma mark - View's lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (!self.initialLayoutWasSet) {
        self.mainLabel.frame = CGRectMake(self.contentInsets.left, self.contentInsets.top, self.utils_width - self.contentInsets.left - self.contentInsets.right, 0);
        [self.mainLabel sizeToFit];
        self.mainLabel.frame = CGRectIntegral(self.mainLabel.frame);
        
        self.secondaryLabel.frame = CGRectMake(self.contentInsets.left, self.mainLabel.utils_maxY + kSecondaryLabelTopSpace, self.utils_width - self.contentInsets.left - self.contentInsets.right, 0);
        [self.secondaryLabel sizeToFit];
        self.secondaryLabel.frame = CGRectIntegral(self.secondaryLabel.frame);
        
        self.showMoreButton.utils_origin = CGPointMake(self.contentInsets.left, self.secondaryLabel.utils_maxY + kVerticalSpace);
        [self.showMoreButton sizeToFit];
        
        self.tableView.utils_y = self.showMoreButton.utils_y;
        self.tableView.utils_size = CGSizeMake(self.utils_width, self.tableView.contentSize.height);

        self.utils_height = self.showMoreButton.utils_maxY + self.contentInsets.bottom;
        
        self.initialLayoutWasSet = YES;
    }
}

#pragma mark - User actions

- (void)showMoreButtonPressed {
    [self.showMoreButton changeRotatableImageButtonState];
    
    // there are situations possible when table view's height is not correct yet
    if (self.tableView.utils_height != self.tableView.contentSize.height) {
        self.tableView.utils_height = self.tableView.contentSize.height;
    }

    CGFloat showMoreInfoButtonY;
    if (self.showingAllInfo) {
        showMoreInfoButtonY = self.tableView.utils_y;
    }
    else {
        showMoreInfoButtonY = self.tableView.utils_maxY + kVerticalSpace;
    }
    
    CGFloat newHeight = showMoreInfoButtonY + self.showMoreButton.utils_height + self.contentInsets.bottom;
    CGFloat duration = .2;
    
    [self.delegate flexibleHeaderView:self willChangeHeightTo:newHeight animatedWithDuration:duration];
    
    [UIView animateWithDuration:.2 animations:^{
        self.utils_height = newHeight;
        self.showMoreButton.utils_y = showMoreInfoButtonY;
        self.tableView.alpha = self.showingAllInfo ? 0 : 1.f;
        
        self.showingAllInfo = !self.showingAllInfo;
    }];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    CGPoint convertedPoint = [self convertPoint:point toView:self.showMoreButton];
    if ([self.showMoreButton pointInside:convertedPoint withEvent:event] && self.showMoreButton.enabled) {
        return self.showMoreButton;
    }
    
    return nil;
}

@end
