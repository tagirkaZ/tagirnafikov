//
//  RotatableImageButton.m
//  BlockChecker
//
//  Created by Tagir Nafikov on 18/12/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

#import "RotatableImageButton.h"
#import "UIView+Utils.h"

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(degrees) ((M_PI * degrees) / 180.0)

@interface RotatableImageButton() {
    UIImageView *_rotatableImageView;
}

@property (nonatomic) NSMutableDictionary *titlesDictionary;

@end

@implementation RotatableImageButton

- (void)sizeToFit {
    [self.rotatableImageView sizeToFit];
    self.titleEdgeInsets = UIEdgeInsetsMake(0, self.rotatableImageView.utils_width + 4.f, 0, 0);
    [self.titleLabel sizeToFit];
    self.utils_height = MAX(self.rotatableImageView.utils_height, self.titleLabel.utils_height);
    self.utils_width = self.titleLabel.utils_width + self.titleEdgeInsets.left;
    self.rotatableImageView.utils_centerY = self.utils_halfHeight;
    self.titleLabel.utils_centerY = self.utils_halfHeight;
}

- (UIImageView *)rotatableImageView {
    if (!_rotatableImageView) {
        _rotatableImageView = [UIImageView new];
        [self addSubview:_rotatableImageView];
    }
    
    return _rotatableImageView;
}

#pragma mark - Public

- (void)setTitle:(NSString *)title forRotatableImageButtonState:(RotatableImageButtonState)state {
    self.titlesDictionary[@(state)] = title;
    
    if (state == self.rotatableImageButtonState) {
        [self setTitle:title forState:UIControlStateNormal];
    }
}

- (void)changeRotatableImageButtonState {
    if (self.rotatableImageButtonState == RotatableImageButtonStateDefault) {
        self.rotatableImageButtonState = RotatableImageButtonStateAlternative;
    }
    else {
        self.rotatableImageButtonState = RotatableImageButtonStateDefault;
    }
}

#pragma mark - Setters/Getters

- (void)setRotatableImageButtonState:(RotatableImageButtonState)rotatableImageButtonState {
    _rotatableImageButtonState = rotatableImageButtonState;
    
    NSString *title = self.titlesDictionary[@(_rotatableImageButtonState)];
    [self setTitle:title forState:UIControlStateNormal];
    
    [self sizeToFit];
    
    CGFloat angle = _rotatableImageButtonState == RotatableImageButtonStateDefault ? 0 : 180.f;
    [UIView animateWithDuration:.2 animations:^{
        self.rotatableImageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(angle));
    }];
}

- (NSDictionary *)titlesDictionary {
    if (!_titlesDictionary) {
        _titlesDictionary = [NSMutableDictionary new];
    }
    
    return _titlesDictionary;
}

- (void)setEnabled:(BOOL)enabled {
    if (enabled) {
        self.titleLabel.alpha = 1.f;
        self.rotatableImageView.alpha = 1.f;
    }
    else {
        self.titleLabel.alpha = .25f;
        self.rotatableImageView.alpha = .25f;
    }
    
    [super setEnabled:enabled];
}

@end
