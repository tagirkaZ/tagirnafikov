//
//  RotatableImageButton.h
//  BlockChecker
//
//  Created by Tagir Nafikov on 18/12/2016.
//  Copyright © 2016 Tagir Nafikov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, RotatableImageButtonState) {
    RotatableImageButtonStateDefault,
    RotatableImageButtonStateAlternative
};

@interface RotatableImageButton : UIButton

@property (nonatomic) RotatableImageButtonState rotatableImageButtonState;
@property (nonatomic, readonly) UIImageView *rotatableImageView;

- (void)changeRotatableImageButtonState;
- (void)setTitle:(NSString *)title forRotatableImageButtonState:(RotatableImageButtonState)state;

@end
