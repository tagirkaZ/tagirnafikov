#import "HijriDate.h"
#import <Foundation/Foundation.h>

@interface HijriCalendar : NSObject

@property (nonatomic, readonly) NSArray *russianMonthNames;
@property (nonatomic, readonly) NSArray *englishMonthNames;
@property (nonatomic, readonly) NSArray *monthNames;
@property (nonatomic, readonly) NSInteger hijriYear;

+ (instancetype)instance;

- (NSString *)russianMonthNameForIndex:(NSInteger)index;
- (NSString *)englishMonthNameForIndex:(NSInteger)index;
- (NSString *)monthNameForIndex:(NSInteger)index;
- (NSString *)russianStringForDate:(HijriDate *)date;
- (NSString *)englishStringForDate:(HijriDate *)date;
- (NSString *)stringForDate:(HijriDate *)date;

- (NSUInteger)numberOfDaysInMonth:(NSInteger)monthNumber;

- (void)setHijriDate:(HijriDate *)hijriDate forDate:(NSDate *)date;
- (HijriDate *)hijriDateForDate:(NSDate *)date;
- (HijriDate *)hijriDateForToday;
- (NSDate *)dateForHijriDate:(HijriDate *)hijriDate;

@end
