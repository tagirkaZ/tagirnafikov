#import "HijriCalendar.h"
#import "TeoriusHelper.h"
#import "SettingsManager.h"
#import "ScheduleManager.h"
#import "SharedClass.h"



NSString *const kHijriCalendarMonthsDaysDict = @"kHijriCalendarMonthsDaysDict";
NSString *const kHijriCalendarPairDate = @"kHijriCalendarPairDate";
NSString *const kHijriCalendarPairHijriDate = @"kHijriCalendarPairHijriDate";
NSString *const kHijriCalendarGregorianToHijriDict = @"kHijriCalendarGregorianToHijriDict";
NSString *const kHijriCalendarHijriToGregorianDict = @"kHijriCalendarHijriToGregorianDict";
NSString *const kHijriCalendarCalendarDict = @"kHijriCalendarCalendarDict";



@interface HijriCalendar() {
    NSCalendar *_calendar;
    NSCalendar *_gregorianCalendar;
    NSArray *_russianMonthNames;
    NSArray *_englishMonthNames;
    NSDictionary *yearMonthsDict;
    NSDictionary *_calendarDict;
}

@property (nonatomic, readonly) NSDictionary *monthsDaysDict;
@property (nonatomic, readonly) NSDictionary *gregorianToHijriDict;
@property (nonatomic, readonly) NSDictionary *hijriToGregorianDict;
@property (nonatomic) NSDictionary *calendarDict;

@property (nonatomic, readonly) NSCalendar *calendar;
@property (nonatomic, readonly) NSCalendar *gregorianCalendar;
@property (nonatomic, readonly) NSDate *now;
@property (nonatomic, readonly) NSInteger year;
@property (nonatomic) NSDateFormatter *dateFormatter;

@end



@implementation HijriCalendar


#pragma mark - Singleton

+ (instancetype)instance {
    static HijriCalendar *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
        
        // TODO для версии 3.13, после Рамадана можно удалить
        if (([[SharedClass sharedClassInstance].versionOfApp isEqualToString:@"3.13"] && ([SharedClass sharedClassInstance].firstLaunchThisVersion || [SharedClass sharedClassInstance].firstLaunch)) || ([[SharedClass sharedClassInstance].versionOfApp isEqualToString:@"3.2"] && [SharedClass sharedClassInstance].firstLaunch)) {
            NSDateComponents *components = [NSDateComponents new];
            components.year = 2016;
            components.month = 6;
            components.day = 6;
            components.hour = 12;
            NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:components];
            HijriDate *hijriDate = [[HijriDate alloc] initWithYear:instance.hijriYear month:9 day:1];
            [instance setHijriDate:hijriDate forDate:date];
        }
    });
    
    return instance;
}


#pragma mark - Dates

- (NSUInteger)numberOfDaysInMonth:(NSInteger)monthNumber {
    return [self.monthsDaysDict[@(monthNumber - 1)] integerValue];
}

- (void)setNumberOfDays:(NSUInteger)numberOfDays forMonth:(NSInteger)month {
    NSMutableDictionary *monthsDaysDict = self.monthsDaysDict.mutableCopy;
    monthsDaysDict[@(month - 1)] = @(numberOfDays);
    
    yearMonthsDict = @{@(self.hijriYear):monthsDaysDict};
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:yearMonthsDict] forKey:kHijriCalendarMonthsDaysDict];
}

- (void)setHijriDate:(HijriDate *)hijriDate forDate:(NSDate *)date {
    if (hijriDate.day == 1) {
        HijriDate *oldHijriDate = [self hijriDateForDate:date];
        if (oldHijriDate.day == 30) {
            [self setNumberOfDays:29 forMonth:oldHijriDate.month];
        }
    }
    
    if (hijriDate.day == 30) {
        [self setNumberOfDays:hijriDate.day forMonth:hijriDate.month];
    }
    
    [self createCalendarWithDate:date andHijriDate:hijriDate];
}

- (HijriDate *)hijriDateForDate:(NSDate *)date {
    NSString *dateKey = [self.dateFormatter stringFromDate:date];
    NSString *hijriDateKey = self.gregorianToHijriDict[dateKey];
    return [self hijriDateFromKey:hijriDateKey];
}

- (HijriDate *)hijriDate:(HijriDate *)hijriDate byAddingDays:(NSInteger)daysToAdd {
    if (daysToAdd == 0) {
        return hijriDate;
    }
    if (daysToAdd > 0) {
        HijriDate *dateToReturn = [HijriDate new];
        NSInteger year = hijriDate.year;
        NSInteger month = hijriDate.month;
        NSInteger day = hijriDate.day;
        NSUInteger daysInMonth = [self numberOfDaysInMonth:month];
        
        for (int daysCounter = 1; daysCounter <= daysToAdd; daysCounter++) {
            day++;
            
            if (day > daysInMonth) {
                day = 1;
                month++;
                if (month > 12) {
                    month = 1;
                    year++;
                }
                daysInMonth = [self numberOfDaysInMonth:month];
            }
        }
        
        dateToReturn.day = day;
        dateToReturn.month = month;
        dateToReturn.year = year;
        
        return dateToReturn;
    }
    if (daysToAdd < 0) {
        HijriDate *dateToReturn = [HijriDate new];
        NSInteger year = hijriDate.year;
        NSInteger month = hijriDate.month;
        NSInteger day = hijriDate.day;
        NSUInteger daysInMonth = [self numberOfDaysInMonth:month];
        
        for (int daysCounter = -1; daysCounter >= daysToAdd; daysCounter--) {
            day--;
            
            if (day <= 0) {
                month--;
                if (month <= 0) {
                    month = 12;
                    year--;
                }
                daysInMonth = [self numberOfDaysInMonth:month];
                day = daysInMonth;
            }
        }
        
        dateToReturn.day = day;
        dateToReturn.month = month;
        dateToReturn.year = year;

        return dateToReturn;
    }
    
    return nil;
}

- (HijriDate *)hijriDateForToday {
    return [self hijriDateForDate:self.now];
}

- (NSDate *)dateForHijriDate:(HijriDate *)hijriDate {
    NSString *key = [self keyForHijriDate:hijriDate];
    NSString *dateString = self.hijriToGregorianDict[key];
    return [self.dateFormatter dateFromString:dateString];
}

- (void)createCalendarWithDate:(NSDate *)date andHijriDate:(HijriDate *)hijriDate {
    // создаем дату 1 января этого года
    NSDateComponents *firstDayComponents = [NSDateComponents new];
    firstDayComponents.year = self.year;
    firstDayComponents.month = 1;
    firstDayComponents.day = 1;
    NSDate *firstDayDate = [self.gregorianCalendar dateFromComponents:firstDayComponents];
    HijriDate *firstDayHijriDate;
    
    // если нет дат, вычисляем стандартную дату по хиджре для 1 января
    if (!date || !hijriDate) {
        NSDateComponents *components = [self.calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:firstDayDate];
        firstDayHijriDate = [HijriDate new];
        firstDayHijriDate.year = components.year;
        firstDayHijriDate.month = components.month;
        firstDayHijriDate.day = components.day;
    }
    else {
        NSInteger startDay = [self.gregorianCalendar ordinalityOfUnit:NSCalendarUnitDay inUnit: NSCalendarUnitEra forDate:[date dateByAddingUTCDifference]];
        NSInteger endDay  = [self.gregorianCalendar ordinalityOfUnit:NSCalendarUnitDay inUnit: NSCalendarUnitEra forDate:[firstDayDate dateByAddingUTCDifference]];
        NSInteger daysDifference = endDay - startDay;
        firstDayHijriDate = [self hijriDate:hijriDate byAddingDays:daysDifference];
    }
    
    NSInteger numberOfDays;
    if (((self.year % 4 == 0) && (self.year % 100 != 0)) || (self.year % 400 == 0)) {
        numberOfDays = 366;
    }
    else {
        numberOfDays = 365;
    }
    
    NSMutableDictionary *hijriToGregorianDict = [[NSMutableDictionary alloc] initWithCapacity:numberOfDays];
    NSMutableDictionary *gregorianToHijriDict = [[NSMutableDictionary alloc] initWithCapacity:numberOfDays];
    
    NSInteger year = firstDayHijriDate.year;
    NSInteger month = firstDayHijriDate.month;
    NSInteger day = firstDayHijriDate.day;
    NSUInteger daysInMonth = [self numberOfDaysInMonth:month];
    
    for (NSUInteger i = 0; i < numberOfDays; i++) {
        NSDate *dayOfYearDate = [firstDayDate dateByAddingTimeInterval:86400 * i];
        HijriDate *dayOfYearHijriDate = [[HijriDate alloc] initWithYear:year month:month day:day];
        
        NSLog(@"%@ – %@", [self.dateFormatter stringFromDate:dayOfYearDate], [self stringForDate:dayOfYearHijriDate]);
        
        NSString *dateKey = [self.dateFormatter stringFromDate:dayOfYearDate];
        NSString *hijriDateKey = [self keyForHijriDate:dayOfYearHijriDate];
        
        hijriToGregorianDict[hijriDateKey] = dateKey;
        gregorianToHijriDict[dateKey] = hijriDateKey;
        
        day++;
        if (day > daysInMonth) {
            day = 1;
            month++;
            if (month > 12) {
                month = 1;
                year++;
            }
            daysInMonth = [self numberOfDaysInMonth:month];
        }
    }
    
    NSDictionary *yearDict1 = @{@(self.year) : hijriToGregorianDict};
    NSDictionary *yearDict2 = @{@(self.year) : gregorianToHijriDict};
    
    self.calendarDict = @{kHijriCalendarHijriToGregorianDict : yearDict1, kHijriCalendarGregorianToHijriDict : yearDict2};
}


#pragma mark - Localized names and dates

- (NSString *)russianMonthNameForIndex:(NSInteger)index {
    index = MIN(11, index);
    index = MAX(0, index);
    return self.russianMonthNames[index];
}

- (NSString *)englishMonthNameForIndex:(NSInteger)index {
    index = MIN(11, index);
    index = MAX(0, index);
    return self.englishMonthNames[index];
}

- (NSString *)monthNameForIndex:(NSInteger)index {
    return [SettingsManager getCurrentlanguage] == InterfaceLanguageRussian ? [self russianMonthNameForIndex:index] : [self englishMonthNameForIndex:index];
}

- (NSString *)russianStringForDate:(HijriDate *)date {
    return [NSString  stringWithFormat:@"%d %@", (int)date.day, [self russianMonthNameForIndex:date.month - 1]];
}

- (NSString *)englishStringForDate:(HijriDate *)date {
    return [NSString  stringWithFormat:@"%@ %d", [self englishMonthNameForIndex:date.month - 1], (int)date.day];
}

- (NSString *)stringForDate:(HijriDate *)date {
    return [SettingsManager getCurrentlanguage] == InterfaceLanguageRussian ? [self russianStringForDate:date] : [self englishStringForDate:date];
}


#pragma mark - Setters/Getters

- (NSCalendar *)calendar {
    if (!_calendar) {
        _calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIslamicCivil];
    }
    
    return _calendar;
}

- (NSCalendar *)gregorianCalendar {
    if (!_gregorianCalendar) {
        _gregorianCalendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    }
    
    return _gregorianCalendar;
}

- (NSDate *)now {
    return [NSDate date];
}

- (NSInteger)hijriYear {
    return [self.calendar component:NSCalendarUnitYear fromDate:self.now];
}

- (NSInteger)year {
    return [self.gregorianCalendar component:NSCalendarUnitYear fromDate:self.now];
}

- (NSDictionary *)monthsDaysDict {
    NSInteger year = self.hijriYear;

    if (yearMonthsDict) {
        NSInteger savedYear = [yearMonthsDict.allKeys.firstObject integerValue];
        if (savedYear == year) {
            return yearMonthsDict[@(savedYear)];
        }
    }
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kHijriCalendarMonthsDaysDict];
    
    if (data) {
        yearMonthsDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    NSDictionary *monthsDaysDict = yearMonthsDict[@(year)];
    
    // если в первый раз или если сменился год
    if (!monthsDaysDict) {
        NSMutableDictionary *monthsDaysDictTemp = [[NSMutableDictionary alloc] initWithCapacity:12];
        
        for (NSUInteger i = 0; i < 12; i++) {
            NSDateComponents *components = [NSDateComponents new];
            components.year = year;
            components.month = i + 1;
            components.day = 10;
            
            NSInteger numberOfDays = [self.calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:[self.calendar dateFromComponents:components]].length;
            monthsDaysDictTemp[@(i)] = @(numberOfDays);
        }
        
        yearMonthsDict = @{@(year):monthsDaysDictTemp};
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:yearMonthsDict] forKey:kHijriCalendarMonthsDaysDict];
        
        return monthsDaysDictTemp.copy;
    }
    
    return monthsDaysDict;
}

- (NSArray *)russianMonthNames {
    if (!_russianMonthNames) {
        _russianMonthNames = @[@"Мухаррам", @"Сафар", @"Рабиу ль-авваль", @"Рабиу с-сани", @"Джумада аль-уля", @"Джумада аль-ахира", @"Раджаб", @"Шаабан", @"Рамадан", @"Шавваль", @"Зуль-каада", @"Зуль-хиджа"];
    }
    
    return _russianMonthNames;
}

- (NSArray *)englishMonthNames {
    if (!_englishMonthNames) {
        _englishMonthNames = @[@"Muharram", @"Safar", @"Rabi-al-awwal", @"Rabi-al-thani", @"Jumada-al-awwal", @"Jumada-al-thani", @"Rajab", @"Shaban", @"Ramadan", @"Shawwal", @"Thul-Qedah", @"Thul-Hijjah"];
    }
    
    return _englishMonthNames;
}

- (NSArray *)monthNames {
    return [SettingsManager getCurrentlanguage] == InterfaceLanguageRussian ? self.russianMonthNames : self.englishMonthNames;
}

- (NSDictionary *)calendarDict {
    if (!_calendarDict) {
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kHijriCalendarCalendarDict];
        
        if (data) {
            _calendarDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        }
        else {
            // для перехода с версии 3.0.1
            NSDate *lastDate = [[NSUserDefaults standardUserDefaults] objectForKey:kHijriCalendarPairDate];
            HijriDate *lastHijriDate;
            NSData *hijriDateData = [[NSUserDefaults standardUserDefaults] objectForKey:kHijriCalendarPairHijriDate];
            
            if (hijriDateData) {
                lastHijriDate = [NSKeyedUnarchiver unarchiveObjectWithData:hijriDateData];
                if (lastHijriDate && !lastHijriDate.year) {
                    lastHijriDate.year = [self.calendar component:NSCalendarUnitYear fromDate:lastDate];
                }
            }
            
            [self createCalendarWithDate:lastDate andHijriDate:lastHijriDate];
        }
    }
    
    return _calendarDict;
}

- (void)setCalendarDict:(NSDictionary *)calendarDict {
    _calendarDict = calendarDict;
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:_calendarDict] forKey:kHijriCalendarCalendarDict];
}

- (NSDictionary *)hijriToGregorianDict {
    NSDictionary *allYearsDict = self.calendarDict[kHijriCalendarHijriToGregorianDict];
    NSDictionary *hijriToGregorianDict = allYearsDict[@(self.year)];
    
    if (!hijriToGregorianDict) {
        // имзенился год
        [self createCalendarWithDate:nil andHijriDate:nil];
    }
    
    return hijriToGregorianDict;
}

- (NSDictionary *)gregorianToHijriDict {
    NSDictionary *allYearsDict = self.calendarDict[kHijriCalendarGregorianToHijriDict];
    NSDictionary *gregorianToHijriDict = allYearsDict[@(self.year)];
    
    if (!gregorianToHijriDict) {
        // имзенился год
        [self createCalendarWithDate:nil andHijriDate:nil];
    }

    return gregorianToHijriDict;
}

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [NSDateFormatter new];
        _dateFormatter.dateFormat = @"dd.MM.yyyy";
    }
    
    return _dateFormatter;
}


#pragma mark - Helpers

- (HijriDate *)hijriDateFromKey:(NSString *)key {
    NSArray *components = [key componentsSeparatedByString:@"_"];
    
    if (components.count != 3) {
        return nil;
    }
    
    HijriDate *date = [HijriDate new];
    date.year = [components[2] integerValue];
    date.month = [components[1] integerValue];
    date.day = [components[0] integerValue];
    
    return date;
}

- (NSString *)keyForHijriDate:(HijriDate *)date {
    return [NSString  stringWithFormat:@"%d_%d_%d", (int)date.day, (int)date.month, (int)date.year];
}


@end
